# github-markdown

Use [GitHub's Markdown API](https://developer.github.com/v3/markdown/) to render a file in Markdown syntax.

**Important**: The code is **not** intended to be used in a public machine. There are no security checks about what files can be read.

## Install

    $ go build
    $ sudo cp github-markdown /usr/local/bin

## Usage

    $ github-markdown file.md

Then, if you want to render the file `file.md`, go to `http://localhost:21500/`
