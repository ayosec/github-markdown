package main

import (
	"io"
	"log"
	"net/http"
	"os"
)

const RENDER = "https://api.github.com/markdown/raw"

var source string

func handler(w http.ResponseWriter, r *http.Request) {

	log.Printf("%s", r.URL.Path)

	file, err := os.Open(source)

	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	defer file.Close()

	resp, err := http.Post(RENDER, "text/x-markdown", file)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	defer resp.Body.Close()

	io.Copy(w, resp.Body)
	w.Write([]byte(`
		<meta charset="UTF-8">
		<style>
			blockquote {
				border-left: 2px solid #ddd;
				padding-left: 1em;
			}
			code {
				background-color: #eee;
				color: black;
			}
		</style>
	`))

}

func main() {
	source = os.Args[1]
	http.HandleFunc("/", handler)
	log.Printf("Listening on 0.0.0.0:21500...")
	log.Fatal(http.ListenAndServe("0.0.0.0:21500", nil))
}
